import { Component } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  public randBg = Math.floor(Math.random() * 3)
  ngOnInit() { 
    $('.openModals').click(function(e){
        e.preventDefault();
        var getAttr = $(this, '[data-attr]').attr('data-attr');
        $("[data-attr]").each(function() {
            if($(this).attr('data-attr') != getAttr){
                $('.' + $(this).attr('data-attr')).addClass('d-none').removeClass('d-block');
            }
        });
        if($('.'+ getAttr).hasClass('d-none')){
            $('.'+ getAttr).toggleClass('d-block').removeClass('d-none');
            let height = 0;
            $('.viewModals span').each(function(i, value){
                height += parseInt($(this).height());
            });

            height += 0;
            $('.viewModals').animate({scrollTop: height});
        }else{
            $('.'+ getAttr).toggleClass('d-none').removeClass('d-block');
        }
    });
  }
}
