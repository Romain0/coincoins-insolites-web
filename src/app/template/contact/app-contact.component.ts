import { Component, OnInit, Inject, Input, ViewEncapsulation } from '@angular/core';
import * as $ from 'jquery';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ContactService } from 'src/app/services/contact.service';


@Component({
    templateUrl: './app-contact.component.html',
    styleUrls: ['./app-contact.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ContactComponent implements OnInit {
    
    public messageSuccess = false;

    contactForm = this.fb.group({
        name:       ['', [Validators.required, Validators.minLength(3)]],
        email:      ['', [Validators.required, Validators.email]],
        subject:    ['', [Validators.required, Validators.minLength(10)]],
        content:    ['', [Validators.required, Validators.minLength(25)]]
    })
    

    constructor(private fb: FormBuilder, private contactService: ContactService) {}

    ngOnInit() {
    
    }


    get f(){
        return this.contactForm.controls;
    }

    submit(){
        if(this.contactForm.status === 'VALID'){
          console.log(this.contactForm.value);
          this.contactService.postMessage(this.contactForm.value).subscribe(response => {
              if(response.status === 200){
                this.resetValue();
                this.messageSuccess = true;
              }
          });
        }
    }

    resetValue(){
        this.contactForm.reset({name: '', email: '', subject: '', content: '', checkbox: 'uncheck'});
    }

}