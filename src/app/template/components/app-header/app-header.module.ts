import { NgModule } from '@angular/core';
import { AppLoginComponent } from '../popup-login/app-login.component';

@NgModule({
    declarations: [
      AppLoginComponent
    ],
})
export class HeaderModule { }