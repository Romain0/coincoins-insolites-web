import { Component, Input, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Validators, FormBuilder } from '@angular/forms';
import { NewsletterService } from 'src/app/services/newsletter.service';

@Component({
    selector: 'app-footer',
    templateUrl: './app-footer.component.html',
    styleUrls: ['./app-footer.component.scss']
})

export class AppFooterComponent implements OnInit {
    public year = new Date().getFullYear();

    public newsletterSuccess = false;

    newsletterForm = this.fb.group({
        email:      ['', [Validators.required, Validators.email]]
    })

    get f(){
        return this.newsletterForm.controls;
    }

    submitEmail(){
        if(this.newsletterForm.status === 'VALID'){
          this.newsletterService.postEmail(this.newsletterForm.value).subscribe(response => {
              if(response.status === 200){
                this.resetValue();
                this.newsletterSuccess = true;
              }
          });
        }
    }

    resetValue(){
        this.newsletterForm.reset({email: ''});
    }

    constructor(private fb: FormBuilder, private newsletterService: NewsletterService) {}

    ngOnInit() { 
        
    }
}