import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})

export class ContactService {
    private urlApi = 'http://localhost:8000/api/contact';  // URL de l'API
    
    constructor(private http: HttpClient) {}
     postMessage(message: any) {
        return this.http.post(this.urlApi, message, { observe: 'response' });
    }
}

