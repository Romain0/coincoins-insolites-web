import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})

export class NewsletterService {
    private urlApi = 'http://localhost:8000/api/newsletter';  // URL de l'API
    
    constructor(private http: HttpClient) {}
     postEmail(email: any) {
        return this.http.post(this.urlApi, email, { observe: 'response' });
    }
}
