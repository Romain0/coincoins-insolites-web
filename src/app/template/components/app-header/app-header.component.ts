import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import * as $ from 'jquery';
import { Location } from '@angular/common';


@Component({
    selector: 'app-header',
    templateUrl: './app-header.component.html',
    styleUrls: ['./app-header.component.scss']
})

export class AppHeaderComponent implements OnInit {
    @Input() title: string;
    
    public route = false;
    public getRoute = null;
    
    constructor(private location: Location){
        this.location.onUrlChange(x => this.urlChange(x));
    }

    ngOnInit() {}

    urlChange(x) {
        this.route = (x === "/home" ? true : false);
    }

}