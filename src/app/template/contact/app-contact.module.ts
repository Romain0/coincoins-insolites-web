import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { ContactComponent } from './app-contact.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
      ContactComponent
    ]
})
export class ContactModule {}